package com.unomic.test;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations= {
		"file:src/main/webapp/WEB-INF/spring/appServlet/servlet-context.xml",
		"file:src/main/webapp/WEB-INF/spring/root-context.xml"
})
public class TestClass implements MqttCallback {
	
	MemoryPersistence persistence = new MemoryPersistence();

	@Test
	public void test() {
		System.out.println("Test Engaing!");
	}
	
	@Test
	public void mqttTest() {
		try {
			MqttClient client = new MqttClient("tcp://52.231.201.192:1883","Subscribing",persistence);
			client.connect();
			client.setCallback(this);
			client.subscribe("1");
			MqttMessage message = new MqttMessage();
			message.setPayload("send my message!!".getBytes());
			client.publish("client", message);
			//client.disconnect();
		}catch(MqttException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void connectionLost(Throwable arg0) {
		// TODO 자동 생성된 메소드 스텁
		
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken arg0) {
		// TODO 자동 생성된 메소드 스텁
		System.out.println("Complete Send Message");
	}

	@Override
	public void messageArrived(String arg0, MqttMessage arg1) throws Exception {
		// TODO 자동 생성된 메소드 스텁
		System.out.println(arg1.toString());
	}
}
