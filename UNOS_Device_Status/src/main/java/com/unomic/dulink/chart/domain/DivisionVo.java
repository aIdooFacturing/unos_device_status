package com.unomic.dulink.chart.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class DivisionVo {

	String division;
	int shopId;
	int amountMin;
}
