package com.unomic.dulink.chart.service;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.unomic.dulink.chart.domain.ChartVo;
import com.unomic.dulink.chart.domain.DivisionVo;

@Service
@Repository
public class ChartServiceImpl extends SqlSessionDaoSupport implements ChartService {
	private final static String namespace = "com.unomic.dulink.chart.";

	@Override
	public String getStartTime(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String rtn = (String) sql.selectOne(namespace + "getStartTime", chartVo);
		return rtn;
	}

	@Override
	public String getComName(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = URLEncoder.encode((String) sql.selectOne(namespace + "getComName", chartVo), "utf-8");
		return str;
	}
	

	@Override
	public ChartVo getBanner(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getBanner", chartVo);
		return chartVo;
	}

	@Override
	public String login(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";

		int exist = (int) sql.selectOne(namespace + "login", chartVo);

		if (exist == 0) {
			str = "fail";
		} else {
			str = "success";
		}
		;

		return str;
	};

	@Override
	public String getFacilitiesStatus(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getFacilitiesStatus", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("name", dataList.get(i).getName());
			map.put("wc", dataList.get(i).getWC());
			map.put("jig", dataList.get(i).getJig());
			map.put("mcTy", dataList.get(i).getMcTy());
			map.put("group", dataList.get(i).getGroup());
			map.put("ncTy", dataList.get(i).getNcTy());
			map.put("cavity", dataList.get(i).getCavity());
			map.put("showing", dataList.get(i).getShowing());
			map.put("pic", dataList.get(i).getPic());
			map.put("w", dataList.get(i).getW());
			map.put("h", dataList.get(i).getH());
			map.put("shopId", dataList.get(i).getShopId());
			map.put("cutLevel", dataList.get(i).getCutLevel());
			map.put("night", dataList.get(i).getNight());
			

			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(list);
		return str;
	}

	@Override
	public String getLampExpression() throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> lampExpressionList = sql.selectList(namespace + "getLampExpression");

		List list = new ArrayList();

		for (int i = 0; i < lampExpressionList.size(); i++) {
			Map map = new HashMap();
			map.put("dvcId", lampExpressionList.get(i).getDvcId());
			map.put("name", lampExpressionList.get(i).getName());
			map.put("redExpression", lampExpressionList.get(i).getRedExpression());
			map.put("redBlinkExpression", lampExpressionList.get(i).getRedBlinkExpression());
			map.put("yellowExpression", lampExpressionList.get(i).getYellowExpression());
			map.put("yellowBlinkExpression", lampExpressionList.get(i).getYellowBlinkExpression());
			map.put("greenExpression", lampExpressionList.get(i).getGreenExpression());
			map.put("greenBlinkExpression", lampExpressionList.get(i).getGreenBlinkExpression());

			list.add(map);
		}
		;

		Map resultMap = new HashMap();
		resultMap.put("dvcId", list);
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);

		return str;
	}

	@Override
	public String setLampExpression(ChartVo chartVo) throws Exception {
		String status = "";

		SqlSession sql = getSqlSession();
		try {
			
			if(chartVo.getExpression()=="") {
				chartVo.setExpression(null);
			}
			
			int check = (int) sql.selectOne(namespace + "getLampExpressionCount", chartVo);
			if (check >= 1) {
				sql.update(namespace + "setLampExpression", chartVo);
			} else {
				sql.update(namespace + "setLampExpression", chartVo);
			}

			status = "success";
		} catch (Exception e) {
			e.printStackTrace();
			status = "fail";
		}

		// TODO 자동 생성된 메소드 스텁
		return status;
	}

	@Override
	public String getJIGList(String shopId) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> jigList = new ArrayList<ChartVo>();
		try {
			jigList = sql.selectList(namespace + "getJIGList", shopId);

			List list = new ArrayList();

			for (int i = 0; i < jigList.size(); i++) {
				Map map = new HashMap();
				map.put("JIG", URLEncoder.encode(jigList.get(i).getJig(), "utf-8"));
				list.add(map);
			}
			;

			Map resultMap = new HashMap();
			resultMap.put("dataList", list);
			String str = "";
			ObjectMapper om = new ObjectMapper();
			str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
			return str;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "fail";

	}

	@Override
	public String getMCTYList(String shopId) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> jigList = new ArrayList<ChartVo>();
		try {
			jigList = sql.selectList(namespace + "getMCTYList", shopId);

			List list = new ArrayList();

			for (int i = 0; i < jigList.size(); i++) {
				Map map = new HashMap();
				map.put("mcTy", URLEncoder.encode(jigList.get(i).getMcTy(), "utf-8"));
				list.add(map);
			}
			;

			Map resultMap = new HashMap();
			resultMap.put("dataList", list);
			String str = "";
			ObjectMapper om = new ObjectMapper();
			str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
			return str;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "fail";
	}

	@Override
	public String updateDevice(String models) throws Exception {
		SqlSession sql = getSqlSession();
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(models);
		ChartVo chartVo = new ChartVo();
		
		chartVo.setName(jsonObj.get("name").toString());
		chartVo.setDvcId(jsonObj.get("dvcId").toString());
		chartVo.setMcTy(jsonObj.get("mcTy").toString());
		chartVo.setJig(jsonObj.get("jig").toString());
		chartVo.setNcTy(jsonObj.get("ncTy").toString());
		chartVo.setWC(jsonObj.get("wc").toString());
		chartVo.setGroup(jsonObj.get("group").toString());
		chartVo.setCavity(jsonObj.get("cavity").toString());
		chartVo.setNight(Integer.parseInt(jsonObj.get("night").toString()));		
		chartVo.setPic(jsonObj.get("pic").toString());
		chartVo.setW(jsonObj.get("w").toString());
		chartVo.setH(jsonObj.get("h").toString());
		chartVo.setCutLevel(Integer.parseInt(jsonObj.get("cutLevel").toString()));
		chartVo.setShowing(jsonObj.get("showing").toString());
		
			
		try {
			System.out.println(chartVo);
			sql.update(namespace + "updateDevice", chartVo);
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}

		return models;
	}

	@Override
	public String getGroup(String shopId) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> jigList = new ArrayList<ChartVo>();
		try {
			jigList = sql.selectList(namespace + "getGroup", shopId);

			List list = new ArrayList();

			for (int i = 0; i < jigList.size(); i++) {
				Map map = new HashMap();
				map.put("group", jigList.get(i).getGroup());
				list.add(map);
			}
			;

			Map resultMap = new HashMap();
			resultMap.put("dataList", list);
			String str = "";
			ObjectMapper om = new ObjectMapper();
			str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
			return str;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "fail";
	}

	@Override
	public String createDevice(String models) throws Exception {
		SqlSession sql = getSqlSession();
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(models);
		ChartVo chartVo = new ChartVo();

		chartVo.setName(jsonObj.get("name").toString());
		chartVo.setMcTy(jsonObj.get("mcTy").toString());
		chartVo.setJig(jsonObj.get("jig").toString());
		chartVo.setNcTy(jsonObj.get("ncTy").toString());
		chartVo.setWC(jsonObj.get("wc").toString());
		chartVo.setGroup(jsonObj.get("group").toString());
		chartVo.setCavity(jsonObj.get("cavity").toString());
		chartVo.setNight(Integer.parseInt(jsonObj.get("night").toString()));
		chartVo.setPic(jsonObj.get("pic").toString());
		chartVo.setH(jsonObj.get("h").toString());
		chartVo.setW(jsonObj.get("w").toString());
		chartVo.setDvcId(jsonObj.get("dvcId").toString());
		chartVo.setShopId(Integer.parseInt(jsonObj.get("shopId").toString()));
		chartVo.setCutLevel(Integer.parseInt(jsonObj.get("cutLevel").toString()));
		chartVo.setShowing(jsonObj.get("showing").toString());
		
		
		try {
			String dvcId = (String) sql.selectOne(namespace + "createDevice", chartVo);
			chartVo.setDvcId(dvcId);
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}

		List list = new ArrayList();
		Map map = new HashMap();
		map.put("dvcId", chartVo.getDvcId());
		map.put("name", chartVo.getName());
		map.put("wc", chartVo.getWC());
		map.put("jig", chartVo.getJig());
		map.put("mcTy", chartVo.getMcTy());
		map.put("group", chartVo.getGroup());
		map.put("ncTy", chartVo.getNcTy());
		map.put("cavity", chartVo.getCavity());
		map.put("showing", chartVo.getShowing());
		map.put("pic", chartVo.getPic());
		map.put("h", chartVo.getH());
		map.put("w", chartVo.getW());
		map.put("dvcId", chartVo.getDvcId());
		map.put("cutLevel", chartVo.getCutLevel());
		

		list.add(map);
		String str = "";

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(list);
		return str;
	}

	@Override
	public void deleteDevice(String models) throws Exception {
		SqlSession sql = getSqlSession();
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(models);
		ChartVo chartVo = new ChartVo();

		chartVo.setDvcId(jsonObj.get("dvcId").toString());
		chartVo.setShopId(Integer.parseInt(jsonObj.get("shopId").toString()));

		try {
			sql.delete(namespace + "deleteDevice", chartVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public String drawCircle(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo> dataList  = sql.selectList(namespace + "drawCircle", chartVo);
		    
		List list = new ArrayList<ChartVo>();
		for(int i = 0; i < dataList.size(); i++){
			Map map = new HashMap();
			
			map.put("id", dataList.get(i).getId());
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("dvcName", URLEncoder.encode(dataList.get(i).getName(),"utf-8"));
			map.put("x", dataList.get(i).getX());
			map.put("y", dataList.get(i).getY());
			map.put("w", dataList.get(i).getWidth());
			map.put("fontSize", dataList.get(i).getFontSize());
			map.put("lastChartStatus", dataList.get(i).getLastChartStatus());
			map.put("noTarget", dataList.get(i).getNoTarget());
			map.put("isChg", dataList.get(i).getIsChg());
			map.put("chgTy", dataList.get(i).getChgTy());
			map.put("type", dataList.get(i).getType());
			map.put("pic", dataList.get(i).getPic());
			
			list.add(map);
		}; 
  
		Map dataMap = new HashMap(); 
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getMachineInfo(ChartVo svgVo) throws Exception {
		SqlSession sql = getSqlSession();
		
		List <ChartVo> machineList = sql.selectList(namespace + "getMachineInfo", svgVo);
		List list=new ArrayList();
		
		for(int i = 0; i < machineList.size(); i++){
			Map map = new HashMap();
			map.put("id", ((ChartVo) machineList.get(i)).getId()); 
			map.put("dvcId", ((ChartVo) machineList.get(i)).getDvcId());
			
			String name = "";
			if(machineList.get(i).getName()!=null){
				name = URLEncoder.encode(machineList.get(i).getName(), "UTF-8");
				name = name.replaceAll("\\+", "%20");
			}; 
			
			map.put("name", name);
			map.put("x", machineList.get(i).getX());
			map.put("y", machineList.get(i).getY());
			map.put("w", machineList.get(i).getW());
			map.put("h", machineList.get(i).getH());
			map.put("fontSize", machineList.get(i).getFontSize());
			map.put("pic", machineList.get(i).getPic());
			map.put("lastChartStatus", machineList.get(i).getLastChartStatus());
			map.put("width", machineList.get(i).getWidth());
			map.put("height", machineList.get(i).getHeight());
			map.put("isChg", machineList.get(i).getIsChg());
			map.put("isChg", machineList.get(i).getIsChg());
			map.put("chgTy", machineList.get(i).getChgTy());
			map.put("type", machineList.get(i).getType());
			map.put("showing", machineList.get(i).getShowing());
			
			list.add(map);
		};
		 
		String str="";
		
		Map resultMap=new HashMap();
		resultMap.put("machineList", list);
		
		ObjectMapper om = new ObjectMapper();
		str=om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		
		return str;
	}

	@Override
	public void setMachinePos(ChartVo svgVo) throws Exception {
		SqlSession sql = getSqlSession();
		sql.update(namespace + "setMachinePos", svgVo);
	}

	@Override
	public void setHide(String id) throws Exception {
		SqlSession sql = getSqlSession();
		
		sql.update(namespace + "setHide", id);
	}

	@Override
	public void setShow(String id) throws Exception {
		// TODO 자동 생성된 메소드 스텁
		SqlSession sql = getSqlSession();
		
		sql.update(namespace + "setShow", id);
	}

	@Override
	public String getPicList(String shopId) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> jigList = new ArrayList<ChartVo>();
		try {
			jigList = sql.selectList(namespace + "getPicList", shopId);

			List list = new ArrayList();

			for (int i = 0; i < jigList.size(); i++) {
				Map map = new HashMap();
				map.put("pic", jigList.get(i).getPic());
				list.add(map);
			}
			;

			Map resultMap = new HashMap();
			resultMap.put("dataList", list);
			String str = "";
			ObjectMapper om = new ObjectMapper();
			str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
			return str;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "fail";
	}

	@Override
	public String getComStartTime(String shopId) throws Exception {
		SqlSession sql = getSqlSession();
		String str = (String) sql.selectOne(namespace + "getComStartTime", shopId);
		return str;
	}
	
	@Override
	public String setComStartTime(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str="failed";
		try {
			str="success";
			sql.update(namespace + "setComStartTime", chartVo);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return str;
	}

	@Override
	public String setComName(ChartVo chartVo) throws Exception {
		String result="fail";
		
		SqlSession sql = getSqlSession();
		try {
			sql.update(namespace+"setComName", chartVo);
			result="success";
		}catch(Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public String setUtc(ChartVo chartVo) throws Exception {
		String result="fail";
		
		SqlSession sql = getSqlSession();
		try {
			sql.update(namespace+"setUtc", chartVo);
			result="success";
		}catch(Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public String getUtc(ChartVo chartVo) throws Exception {
		String result="fail";
		
		SqlSession sql = getSqlSession();
		try {
			result = (String) sql.selectOne(namespace+"getUtc", chartVo);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public String getUserList(String shopId) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getUserList", shopId);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("IDX", dataList.get(i).getIdx());
			map.put("name", dataList.get(i).getId());
			map.put("password", dataList.get(i).getPassword());
			map.put("level", dataList.get(i).getLv());
			map.put("shopId", dataList.get(i).getShopId());
			list.add(map);
		};

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(list);
		return str;
	}

	@Override
	public String logout(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		
		try {
			sql.update(namespace+"logout", chartVo);
		}catch(Exception e) {
			e.printStackTrace();
			return "failed";
		}
		
		return "success";
	}

	@Override
	public void deleteUser(String models) throws Exception {
		SqlSession sql = getSqlSession();
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(models);
		ChartVo chartVo = new ChartVo();
		chartVo.setIdx(jsonObj.get("IDX").toString());
		chartVo.setName(jsonObj.get("name").toString());
		chartVo.setPassword(jsonObj.get("password").toString());
		chartVo.setLv(jsonObj.get("level").toString());

		try {
			sql.delete(namespace + "deleteUser", chartVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public String updateUser(String models) throws Exception {
		SqlSession sql = getSqlSession();
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(models);
		ChartVo chartVo = new ChartVo();
		
		chartVo.setId(jsonObj.get("name").toString());
		chartVo.setPassword(jsonObj.get("password").toString());
		chartVo.setLv(jsonObj.get("level").toString());
		chartVo.setIdx(jsonObj.get("IDX").toString());
		chartVo.setShopId(Integer.parseInt(jsonObj.get("shopId").toString()));

		try {
			sql.update(namespace + "updateUser", chartVo);
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}

		return models;
	}
	
	@Override
	public String createUser(String models) throws Exception {
		
		
		SqlSession sql = getSqlSession();
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(models);
		ChartVo chartVo = new ChartVo();

		chartVo.setId(jsonObj.get("name").toString());
		chartVo.setPassword(jsonObj.get("password").toString());
		chartVo.setLv(jsonObj.get("level").toString());
		chartVo.setShopId(Integer.parseInt(jsonObj.get("shopId").toString()));
		
		try {
			String idx = (String) sql.selectOne(namespace + "createUser", chartVo);
			chartVo.setIdx(idx);
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}

		List list = new ArrayList();
		Map map = new HashMap();
		map.put("name", chartVo.getId());
		map.put("password", chartVo.getPassword());
		map.put("level", chartVo.getLv());
		map.put("shopId", chartVo.getShopId());
		map.put("idx", chartVo.getIdx());
		

		list.add(map);
		String str = "";

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(list);
		return str;
	}

	@Override
	public String updateUserBasic(ChartVo chartVo) throws Exception {
		
		SqlSession sql = getSqlSession();
		String str ="success";
		try {
			sql.update(namespace+"updateUserBasic", chartVo);
		}catch(Exception e) {
			e.printStackTrace();
			str="failed";
		}
		
		return str;
	}

	@Override
	public String getStatusExpression(String dvcId) throws Exception {
		SqlSession sql = getSqlSession();
		
		List <ChartVo> stateList = sql.selectList(namespace + "getStatusExpression",dvcId);
		List list=new ArrayList();
		
		for(int i = 0; i < stateList.size(); i++){
			Map map = new HashMap();
			map.put("type", ((ChartVo) stateList.get(i)).getType()); 
			map.put("status", ((ChartVo) stateList.get(i)).getStatus());
			map.put("statement", ((ChartVo) stateList.get(i)).getStatement());
			list.add(map);
		};
		 
		String str="";
		
		Map resultMap=new HashMap();
		resultMap.put("statementList", list);
		
		ObjectMapper om = new ObjectMapper();
		str=om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		
		return str;
	}

	@Override
	public String updateStatus(ChartVo chartVo) throws Exception {
		String result="fail";
		System.out.println(chartVo.getStatus());
		System.out.println(chartVo);
		System.out.println(chartVo.getExpression());
		SqlSession sql = getSqlSession();
		try {
			if(chartVo.getStatus().equals("IN-CYCLE")) {
				sql.update(namespace+"updateStatus_Incycle", chartVo);
			}else if(chartVo.getStatus().equals("CUT")) {
				sql.update(namespace+"updateStatus_Cut", chartVo);
			}else if(chartVo.getStatus().equals("WAIT")) {
				sql.update(namespace+"updateStatus_Wait", chartVo);
			}else if(chartVo.getStatus().equals("ALARM")) {
				sql.update(namespace+"updateStatus_Alarm", chartVo);
			}
			
			result="success";
		}catch(Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public String getDvcList(String shopId) throws Exception {
		String result="fail";
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getDvcList", shopId);

		List list = new ArrayList();
		
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("name", dataList.get(i).getName());
			map.put("dvcId", dataList.get(i).getDvcId());
			list.add(map);
		};

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(list);
		return str;
		
	}

	@Override
	public String setDivision(String list) throws Exception {
		// TODO 자동 생성된 메소드 스텁
		SqlSession sql = getSqlSession();
		
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(list);
		JSONArray jsonArray = (JSONArray)obj;
		List<DivisionVo> inputList = new ArrayList<DivisionVo>();
		
		String result = "failed";
		
		try {
			for(int i=0;i<jsonArray.size();i++) {
				DivisionVo division = new DivisionVo();
				JSONObject jsonObj = (JSONObject)jsonArray.get(i);
				division.setDivision(jsonObj.get("division").toString());
				division.setShopId(Integer.parseInt(jsonObj.get("shopId").toString()));
			/*	division.setSTime(jsonObj.get("sTime").toString());
				division.setETime(jsonObj.get("eTime").toString());*/
				division.setAmountMin(Integer.parseInt(jsonObj.get("amountMin").toString()));
				inputList.add(division);
			}
			
			int check = (int) sql.selectOne(namespace + "getDevisionList", inputList.get(0).getShopId());
			if(check>0) {
				sql.update(namespace + "updateDevisionList", inputList);
			}else {
				sql.insert(namespace + "setDevision", inputList);
			}
			sql.delete(namespace + "deleteProduction", inputList.get(0).getShopId());
			result="success";
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		
		
		return result;
	}

	@Override
	public String getDivision(String shopId) throws Exception {
		SqlSession sql = getSqlSession();
		List<DivisionVo> dataList = sql.selectList(namespace + "getDivision", shopId);

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", dataList);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataList);
		return str;
	}

};