package com.unomic.dulink.chart.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.unomic.dulink.chart.domain.ChartVo;
import com.unomic.dulink.chart.service.ChartService;

/**
 * Handles requests for the application home page.
 */
/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/")
@Controller
public class ChartController {

	private static final Logger logger = LoggerFactory.getLogger(ChartController.class);
	final int shopId = 1;
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@Autowired
	private ChartService chartService;

	@RequestMapping(value = "index")
	public String alarmReport() {
		return "chart/index";
	};
	
	@RequestMapping(value = "rotation")
	public String rotation() {
		return "chart/rotation";
	};

	@RequestMapping(value = "account")
	public String account() {
		return "chart/account";
	};
	
	@RequestMapping(value = "appDownload")
	public String appDownload() {
		return "chart/appDownload";
	};

	@RequestMapping(value = "account_Setting")
	public String account_Setting() {
		return "chart/account_Setting";
	};

	@RequestMapping(value = "status-Setting")
	public String status_Setting() {
		return "chart/status-Setting";
	};

	@RequestMapping(value = "layout_Setting")
	public String layout_Setting() {
		return "chart/layout-Setting";
	};

	@RequestMapping(value = "fileUpDown")
	public String fileUpDown() {
		return "chart/fileUpDown";
	};
	
	@RequestMapping(value = "ETC_Setting")
	public String ETC_Setting() {
		return "chart/ETC-Setting";
	};

	@RequestMapping(value = "drawCircle")
	@ResponseBody
	public String drawCircle(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.drawCircle(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "setMachinePos")
	@ResponseBody
	public void setMachinePos(ChartVo chartVo) {
		try {
			chartService.setMachinePos(chartVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	};

	@RequestMapping(value = "setHide")
	@ResponseBody
	public void setHide(String id) {
		try {
			chartService.setHide(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
	};

	@RequestMapping(value = "setShow")
	@ResponseBody
	public void setShow(String id) {
		try {
			chartService.setShow(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
	};

	@RequestMapping(value = "getMachineInfo")
	@ResponseBody
	public String getMachineInfo(ChartVo svgVo) {
		String machine = "";
		try {
			machine = chartService.getMachineInfo(svgVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		;

		return machine;
	};

	@RequestMapping(value = "getFacilitiesStatus", produces = "text/html; charset=UTF8")
	@ResponseBody
	public Object getFacilitiesStatus(ChartVo chartVo) {
		String str = "";

		try {
			str = chartService.getFacilitiesStatus(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/html; charset=utf-8");
		return new ResponseEntity(str, responseHeaders, HttpStatus.CREATED);
		// return str;

	}

	@RequestMapping(value = "getUserList", produces = "text/html; charset=UTF8")
	@ResponseBody
	public Object getUserList(String shopId) {
		String str = "";

		try {
			str = chartService.getUserList(shopId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/html; charset=utf-8");
		return new ResponseEntity(str, responseHeaders, HttpStatus.CREATED);
		// return str;

	}

	float progress = 0;

	@RequestMapping(value = "resetProgress")
	@ResponseBody
	public void resetProgress() {
		progress = 0;
	};

	@RequestMapping(value = "getProgress")
	@ResponseBody
	public float getProgress() {
		// logger.info(progress);
		return progress;
	};

	@RequestMapping(value = "login")
	@ResponseBody
	public String loginCnt(ChartVo chartVo) {
		String str = "failed";
		try {
			str = chartService.login(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getStartTime")
	@ResponseBody
	public String getStartTime(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getStartTime(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};

	@RequestMapping(value = "getComName")
	@ResponseBody
	public String getComName(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getComName(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "setComName")
	@ResponseBody
	public String setComName(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.setComName(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getComStartTime")
	@ResponseBody
	public String getComStartTime(String shopId) {
		String str = "";
		try {
			str = chartService.getComStartTime(shopId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "setComStartTime")
	@ResponseBody
	public String setComStartTime(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.setComStartTime(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getJIGList")
	@ResponseBody
	public String getJIGList(String shopId) {
		String str = "";
		try {
			str = chartService.getJIGList(shopId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getPicList")
	@ResponseBody
	public String getPicList(String shopId) {
		String str = "";
		try {
			str = chartService.getPicList(shopId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getMCTYList")
	@ResponseBody
	public String getMCTYList(String shopId) {
		String str = "";
		try {
			str = chartService.getMCTYList(shopId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getGroup")
	@ResponseBody
	public String getGroup(String shopId) {
		String str = "";
		try {
			str = chartService.getGroup(shopId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "updateDevice",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String updateDevice(String models) {
		try {
			logger.info(models);
			models = chartService.updateDevice(models);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return models;
	}

	@RequestMapping(value = "createDevice", produces = "text/html; charset=UTF8")
	@ResponseBody
	public Object createDevice(String models) {
		try {
			models = chartService.createDevice(models);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/html; charset=utf-8");
		return new ResponseEntity(models, responseHeaders, HttpStatus.CREATED);
	}

	@RequestMapping(value = "deleteDevice")
	@ResponseBody
	public void deleteDevice(String models) {
		try {
			chartService.deleteDevice(models);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@RequestMapping(value = "deleteUser")
	@ResponseBody
	public void deleteUser(String models) {
		try {
			chartService.deleteUser(models);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@RequestMapping(value = "getBanner")
	@ResponseBody
	public ChartVo getBanner(ChartVo chartVo) {
		try {
			chartVo = chartService.getBanner(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return chartVo;
	}

	@RequestMapping(value = "lampExpression")
	public String LampExpression() {
		return "chart/lampExpression";
	};

	@RequestMapping(value = "getLampExpression")
	@ResponseBody
	public String getLampExpression() {
		String str = "";
		try {
			str = chartService.getLampExpression();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};

	@RequestMapping(value = "setLampExpression")
	@ResponseBody
	public String setLampExpression(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.setLampExpression(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			str = "fail";
		}
		return str;
	};

	@RequestMapping(value = "setUtc")
	@ResponseBody
	public String setUtc(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.setUtc(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			str = "fail";
		}
		return str;
	};

	@RequestMapping(value = "getStatusExpression")
	@ResponseBody
	public String getStatusExpression(String dvcId) {
		String str = "";
		try {
			str = chartService.getStatusExpression(dvcId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			str = "fail";
		}
		return str;
	};

	@RequestMapping(value = "getUtc")
	@ResponseBody
	public String getUtc(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getUtc(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			str = "fail";
		}
		return str;
	};

	@RequestMapping(value = "updateUser")
	@ResponseBody
	public String updateUser(String models) {
		try {
			models = chartService.updateUser(models);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return models;
	}

	@RequestMapping(value = "updateUserBasic")
	@ResponseBody
	public String updateUserBasic(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.updateUserBasic(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			str = "failed";
		}
		return str;
	}

	@RequestMapping(value = "createUser", produces = "text/html; charset=UTF8")
	@ResponseBody
	public Object createUser(String models) {
		try {
			models = chartService.createUser(models);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/html; charset=utf-8");
		return new ResponseEntity(models, responseHeaders, HttpStatus.CREATED);
	}

	@RequestMapping(value = "fileUpload")
	@ResponseBody
	public String fileUp(MultipartHttpServletRequest multi) {

		String root = multi.getSession().getServletContext().getRealPath("/");
		String path = root + "resources/upload/";

		String newFileName = "";

		logger.info(""+multi.getSession().getServletContext());

		File dir = new File(path);
		if (!dir.isDirectory()) {
			dir.mkdir();
		}

		Iterator<String> itr = multi.getFileNames();
		if (itr.hasNext()) {
			MultipartFile mpf = multi.getFile(itr.next());
			logger.info(mpf.getOriginalFilename() + " uploaded!");
			try { // just temporary save file info into ufile
				logger.info("file length : " + mpf.getBytes().length);
				logger.info("file name : " + mpf.getOriginalFilename());

				logger.info(path);

				newFileName = "road" + "."
						+ mpf.getOriginalFilename().substring(mpf.getOriginalFilename().lastIndexOf(".") + 1);

				mpf.transferTo(new File(path + newFileName));

			} catch (IOException e) {
				logger.info(e.getMessage());
				e.printStackTrace();
			}
			return path;
		} else {
			return "failed";
		}

	}

	@RequestMapping(value = "fileUpload_Device")
	@ResponseBody
	public String fileUpload_Device(MultipartHttpServletRequest multi) {
		
		String root = multi.getSession().getServletContext().getRealPath("/");
		String path = root + "resources/files/"+multi.getParameter("dvcId")+"/";

		String newFileName = "";

		File dir = new File(path);
		if (!dir.isDirectory()) {
			dir.mkdir();
		}

		Iterator<String> itr = multi.getFileNames();
		if (itr.hasNext()) {
			MultipartFile mpf = multi.getFile(itr.next());
			try { // just temporary save file info into ufile
				
				mpf.transferTo(new File(path + mpf.getOriginalFilename()));
				
				Path file_path = Paths.get(path + mpf.getOriginalFilename());
				
				MqttClient client = new MqttClient("tcp://52.231.201.192:1883","Subscribing");
				client.connect();
				client.subscribe(multi.getParameter("dvcId"));
				
				MqttMessage message = new MqttMessage();
				message.setPayload(Files.readAllBytes(file_path));
				client.publish("client", message);
				client.disconnect();
				
			} catch (IOException | MqttException e) {
				logger.info(e.getMessage());
				e.printStackTrace();
			}
			
			return path;
		} else {
			return "failed";
		}

	}
	
	@RequestMapping(value = "imageUpload_Device")
	@ResponseBody
	public String imageUpload_Device(MultipartHttpServletRequest multi) {
		
		
		String root = multi.getSession().getServletContext().getRealPath("/");
		String path = root + "resources/upload/";
		
		String newFileName = "";
		
		File dir = new File(path);
		if (!dir.isDirectory()) {
			dir.mkdir();
		}
		
		Iterator<String> itr = multi.getFileNames();
		if (itr.hasNext()) {
			MultipartFile mpf = multi.getFile(itr.next());
			try { // just temporary save file info into ufile
				mpf.transferTo(new File(path + mpf.getOriginalFilename()));
			} catch (IOException e) {
				logger.info(e.getMessage());
				e.printStackTrace();
			}
			return path;
		} else {
			return "failed";
		}
		
	}

	@RequestMapping(value = "getFileList")
	@ResponseBody
	public String getFileList(HttpServletRequest request, @RequestParam("dvcId")String dvcId) throws Exception {
		String root = request.getSession().getServletContext().getRealPath("/");
		String path = root + "resources/files/"+dvcId+"/";

		logger.info(path);
		
		List list = new ArrayList();

		List resultList = new ArrayList();

		File dirFile = new File(path);
		File[] fileList = dirFile.listFiles();

		for (File tempFile : fileList) {
			if (tempFile.isFile()) {
				String tempPath = tempFile.getParent();
				String tempFileName = tempFile.getName();
				logger.info(tempFileName);
				String[] array = tempFileName.split("_");
				
				Map map = new HashMap();
				map.put("file", tempFileName);
				list.add(map);
				
			}
		}

		for (int i = 0; i < list.size(); i++) {
			if (!resultList.contains(list.get(i))) {
				resultList.add(list.get(i));
			}
		}

		String str = "";

		Map resultMap = new HashMap();
		resultMap.put("fileList", resultList);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);

		return str;

	}
	
	@RequestMapping(value = "getImageList")
	@ResponseBody
	public String getImageList(HttpServletRequest request) throws Exception {
		String root = request.getSession().getServletContext().getRealPath("/");
		String path = root + "resources/upload/";
		
		List list = new ArrayList();
		
		List resultList = new ArrayList();
		
		File dirFile = new File(path);
		File[] fileList = dirFile.listFiles();
		
		for (File tempFile : fileList) {
			if (tempFile.isFile()) {
				String tempPath = tempFile.getParent();
				String tempFileName = tempFile.getName();
				
				String[] array = tempFileName.split("_");
				
				if (array[0].contains(".svg") || array[0].contains(".png") || array[0].contains(".jpg")) {
					
				} else {
					Map map = new HashMap();
					map.put("file", array[0]);
					list.add(map);
				}
			}
		}
		
		for (int i = 0; i < list.size(); i++) {
			if (!resultList.contains(list.get(i))) {
				resultList.add(list.get(i));
			}
		}
		
		String str = "";
		
		Map resultMap = new HashMap();
		resultMap.put("fileList", resultList);
		
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		
		return str;
		
	}

	@RequestMapping(value = "checkStatusFile")
	@ResponseBody
	public String checkStatusFile(String fileName, HttpServletRequest request) throws Exception {
		String root = request.getSession().getServletContext().getRealPath("/");
		String path = root + "resources/upload/";

		File dirFile = new File(path);
		File[] fileList = dirFile.listFiles();

		String[] condition = { "ALARM", "ALARM-NOBLINK", "IN-CYCLE", "IN-CYCLE-BLINK", "WAIT", "WAIT-NOBLINK",
				"NO-CONNECTION" };
		
		List hasCondition = new ArrayList();

		int existCheck = 0;

		for (File tempFile : fileList) {
			if (tempFile.isFile()) {
				String tempFileName = tempFile.getName();
				String[] array = tempFileName.split("_");

				if (array[0].contains(".svg") || array[0].contains(".png") || array[0].contains(".jpg")) {

				} else {
					if (array[0].equals(fileName)) {
						String[] subArray = array[1].split("\\.");
						
						for (int i = 0; i < condition.length; i++) {
							if (subArray[0].equals(condition[i])) {
								
								existCheck++;
								
								hasCondition.add(condition[i]);
								
							}
						}
					};
				}
			}
		}

		
		List resultList = new ArrayList();
		
		
		for(int i=0;i<hasCondition.size();i++) {
			if(!resultList.contains(hasCondition.get(i))) {
				resultList.add(hasCondition.get(i));
			}
		}
		
		
		Map map = new HashMap();
		map.put("fileName", fileName);
		map.put("hasCondition", resultList);

		JSONObject json = new JSONObject();
		json.putAll(map);
		
		return json.toJSONString();
		
	}
	
	@RequestMapping(value = "updateStatus")
	@ResponseBody
	public String updateStatus(ChartVo chartVo) {
		
		String result="";
		
		try {
			result = chartService.updateStatus(chartVo);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	@RequestMapping(value = "getDvcList")
	@ResponseBody
	public String getDvcList(String shopId) {
		
		String result="";
		
		
		try {
			result = chartService.getDvcList(shopId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	@RequestMapping(value = "setDivision")
	@ResponseBody
	public String setDivision(String list) {
		
		String result="";
		
		try {
			result = chartService.setDivision(list);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	@RequestMapping(value = "getDivision")
	@ResponseBody
	public String getDivision(String shopId) {
		
		String result="";
		
		try {
			result = chartService.getDivision(shopId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

};
