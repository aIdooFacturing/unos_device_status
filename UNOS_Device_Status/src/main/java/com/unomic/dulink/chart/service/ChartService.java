package com.unomic.dulink.chart.service;

import java.util.List;

import com.unomic.dulink.chart.domain.ChartVo;

public interface ChartService {
	public String getFacilitiesStatus(ChartVo chartVo) throws Exception;

	public String login(ChartVo chartVo) throws Exception;

	public String getStartTime(ChartVo chartVo) throws Exception;

	public String getComName(ChartVo chartVo) throws Exception;

	public ChartVo getBanner(ChartVo chartVo) throws Exception;


	public String getLampExpression() throws Exception;

	public String setLampExpression(ChartVo chartVo) throws Exception;

	public String getJIGList(String shopId) throws Exception;

	public String getMCTYList(String shopId) throws Exception;

	public String updateDevice(String models) throws Exception;

	public String getGroup(String shopId) throws Exception;

	public String createDevice(String models) throws Exception;

	public void deleteDevice(String models) throws Exception;

	public String drawCircle(ChartVo chartVo) throws Exception;

	public String getMachineInfo(ChartVo svgVo) throws Exception;

	public void setMachinePos(ChartVo chartVo) throws Exception;

	public void setHide(String id) throws Exception;

	public void setShow(String id) throws Exception;

	public String getPicList(String shopId) throws Exception;

	public String getComStartTime(String shopId) throws Exception;

	public String setComName(ChartVo chartVo) throws Exception;

	public String setUtc(ChartVo chartVo) throws Exception;

	public String getUtc(ChartVo chartVo) throws Exception;

	public String getUserList(String shopId) throws Exception;

	public String logout(ChartVo chartVo) throws Exception;

	public void deleteUser(String models) throws Exception;

	public String updateUser(String models) throws Exception;

	public String createUser(String models) throws Exception;

	public String updateUserBasic(ChartVo chartVo) throws Exception;

	public String setComStartTime(ChartVo chartVo) throws Exception;

	public String getStatusExpression(String dvcId) throws Exception;

	public String updateStatus(ChartVo chartVo) throws Exception;

	public String getDvcList(String shopId) throws Exception;

	public String setDivision(String list) throws Exception;

	public String getDivision(String shopId) throws Exception;

};
