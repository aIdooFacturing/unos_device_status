
package com.unomic.dulink.chart.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ChartVo {
	String jig;
	String mcTy;
	String group;
	String ncTy;
	String WC;

	// common
	String ty;
	String fileLocation;
	String url;
	String fileName;
	String appId;
	String appName;
	String categoryId;
	Integer shopId;
	String dvcId;
	String id;
	String pwd;
	String name;
	String msg;
	String rgb;
	
	private String userid;
	private int count;
	private int red;
	private int redBlink;
	private int yellow;
	private int yellowBlink;
	private int green;
	private int greenBlink;
	private String address;
	private String addressValue;
	
	String redExpression;
	String redBlinkExpression;
	String yellowExpression;
	String yellowBlinkExpression;
	String greenExpression;
	String greenBlinkExpression;
	
	String expressionName;
	String expression;
	String idx;
	
	String time;
	String cavity;
	
	String x;
	String y;
	String fontSize;
	String width;
	String lastChartStatus;
	String noTarget;
	String isChg;
	String chgTy;
	String type;
	String pic;
	String w;
	String h;
	String height;
	
	String showing;
	String startTime;
	
	String utc;
	
	String password;
	String lv;
	
	String status;
	String statement;
	
	String file;
	int cutLevel;
	
	int night;
}
