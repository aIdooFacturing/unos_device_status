package com.unomic.dulink.chart.controller;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping(value = "/mqtt")
@Controller
public class MqttControll implements MqttCallback {
	
	MemoryPersistence persistence = new MemoryPersistence();

	@RequestMapping(value = "/index")
	@ResponseBody
	public void mqttConnect() {
		try {
			MqttClient client = new MqttClient("tcp://52.231.201.192:1883","Subscribing",persistence);
			client.connect();
			client.setCallback(this);
			client.subscribe("1"); //subcribe는 dvcId 값으로 매핑한다.
			/*MqttMessage message = new MqttMessage();
			message.setPayload("I am Connected.".getBytes());
			client.publish("client", message);*/
			//client.disconnect();
		}catch(MqttException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void connectionLost(Throwable cause) {
		// TODO 자동 생성된 메소드 스텁
		System.out.println("Connection Lost");
	}

	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		// TODO 자동 생성된 메소드 스텁
		System.out.println("Message has Arrive : " + message);
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		// TODO 자동 생성된 메소드 스텁
		System.out.println("delivery Complete");
	}

}
