<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.common.min.css"/>
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.rtl.min.css"/>
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.silver.min.css"/>
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<script src="https://kendo.cdn.telerik.com/2018.1.221/js/kendo.all.min.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow: hidden;
	background-color: black;
  	font-family:'Helvetica';
}
#wraper{
	height : 80%;
}
.mainView{
	overflow: hidden;
	overflow-x:hidden;
}
.k-grid tbody > tr
{
 background : #323232 !important;
 text-align: center;
 color: white !important;
}

.k-grid tbody > tr:hover
{
 background :  darkgray !important;
}
 
.k-grid tbody > .k-alt
{
 background :  #222222	 !important;
 text-align: center;
 color: white;
}
.k-grid-content{
 background : black;
}

.k-grid{
	border : 2px solid #323232;
	border-bottom : 0px;
}
.k-grid th.k-header,
.k-grid-header
{
    background: linear-gradient( to bottom, gray, black ) !important;
    color : white !important;
    text-align: center;
}
div.k-grid-header{
 padding-right:0px !important;
}

</style> 
<script type="text/javascript">
	
	$(function(){
		
		getToday();
		
		createNav("config_nav", 1);
		setEl();
		
		if(window.sessionStorage.getItem("level")!='2'){
			alert("권한이 없습니다.");
			window.location.href='/DMT/chart/index.do';
		}
		
		var lang = window.localStorage.getItem("lang");
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999,
			"pointer-events": "none"
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"text-align" : "center",
			"color" : "white",
			"font-size" : getElSize(35),
			"padding" : getElSize(20),
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$("#wraper").css({
			"width" : "100%"
		})
		
		$(".tmpTable").css({
			"width" : "100%"			
		})
		
		$("#wraper").css({
			"height" : getElSize(1750)
		})
		
		$("#id-div").css({
			"position" : "absolute",
			"top" : getElSize(300),
			"left" : getElSize(600)
		})
		
		$("#id-input").val(window.sessionStorage.getItem("user_id"));
	
		$("#id-input").css({
			"font-size" : getElSize(100),
			"width" : getElSize(500)
		})
		
		$("#id-label").css({
			"color" : "white",
			"font-size" : getElSize(100)
		})
		
		$("#btn-div").css({
			"position" : "absolute",
			"bottom" : getElSize(300),
			"left" : getElSize(600),
			"width" : getElSize(2950)
		})
		
		$("#delete-btn").css({
			"font-size" : getElSize(100),
			"float" : "right"
		})
		
		$("#change-btn").css({
			"font-size" : getElSize(100)
		})
		
		$(".input-div").css({
			"height" : "50%"
		})
		
		if(getParameterByName('lang')=='ko' || window.localStorage.getItem("lang")=="ko"){
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(100),
				"position" : "absolute",
				"top" : marginHeight + getElSize(85),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else if(getParameterByName('lang')=='en' || getParameterByName('lang')=='de' || window.localStorage.getItem("lang")=="de" || window.localStorage.getItem("lang")=="en"){
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(70),
				"position" : "absolute",
				"top" : marginHeight + getElSize(115),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else{
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(90),
				"position" : "absolute",
				"top" : marginHeight + getElSize(100),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}
		
	}
	
	function showSelector(container, options, a){

		$('<input data-bind="value:' + options.field + '"/>')
		.appendTo(container)
		.kendoDropDownList({
			 dataSource: [
				    { id: 1, name: "${user}" },
				    { id: 2, name: "${manager}" },
		  	],
		  	dataTextField: "name",
		    dataValueField: "id",
	        select:function(e){
	        }
	    }).data("kendoDropDownList");
	}
	
	
	$(function(){
		
		kendodata = new kendo.data.DataSource({
			 transport: {
	            read: {
	            	 url: ctxPath + "/getUserList.do?shopId="+shopId,
	                 dataType: "json",
	                 type: "GET",
	                 data:function(e){
	                	 console.log(e)
	                 }
	            },
	            destroy: {
	            	url: ctxPath + "/deleteUser.do",
	                dataType: "json",
	                type: "POST",
	            },
	            update: {
	            	url: ctxPath + "/updateUser.do",
	            	dataType: "json"
	            },
	            create: {
	            	url: ctxPath + "/createUser.do",
	            	dataType: "json"
	            },
	            parameterMap: function(options, operation) {
	            	console.log(options)
					return {
						models: kendo.stringify(options)
						};
	            }
	        },
	       /*  change: function(e){
	            if(e.action == "itemchange"){
	                e.items[0].dirtyFields = e.items[0].dirtyFields || {};
	                e.items[0].dirtyFields[e.field] = true;
	            }
	        }, */
	        batch: false,
	       schema: {
	           model: {
	               id: "IDX",
	               fields: {
	               	name: { editable: true, nullable: false,  
	               		validation: {
	                        required: true,
	                        maxlength: 10
                    	} 
	        		},
	                password: { editable: true, nullable: false,
	                	validation: {
	                        required: true,
	                        maxlength: 12
                    	}
	                },
	                level: { defaultValue: 1, editable: true, nullable: false, type: "number"},
	                shopId : { defaultValue: 1, type: "number" }
	               }
	           }
	       }
		})
		
		
		
		
		
		
		if(window.sessionStorage.getItem("level")=='2'){
			grid =  $("#wraper").kendoGrid({
				dataSource : kendodata,
				height : getElSize(1780),
				scrollable:true,
				editable: { 
					mode: "inline",
					confirmation: "${askDeletion}"
				},
				toolbar: [{ name: "create" , text:'${id} ${add}'}],
				 columns: [
					 {
					    field: "name",
					    title: "${id}",
					    width : getElSize(400),
					    attributes: {
			        	      style: "font-size:"+getElSize(40),
			      	    },headerAttributes: {
			      	    	 style: "font-size:"+getElSize(40),
			      	    }
					  },{
					    field: "password",
					    title: "${password}",
					    width : getElSize(400),
					    attributes: {
			        	      style: "font-size:"+getElSize(40),
			      	    },headerAttributes: {
			      	    	 style: "font-size:"+getElSize(40),
			      	    }
					  },{
					    field: "level",
					    title: "${autority}",
					    template : function(dataItem){
					    	if(dataItem.level==1){
					    		return "${user}"
					    	}else if(dataItem.level==2){
					    		return "${manager}"
					    	}
					    },
					    width : getElSize(400),
					    editor: showSelector,
					    attributes: {
			        	      style: "font-size:"+getElSize(40),
			      	    },headerAttributes: {
			      	    	 style: "font-size:"+getElSize(40),
			      	    }
					  },
					  { command: [{ name: "edit", text: { update: '${modify} ${compl}', edit : '${modify}',cancel: "${modify} ${cancel}",} }, { name: "destroy", text: '${del}' }], title: "&nbsp;", width: getElSize(450) }
				  ]
			 }).data("kendoGrid");
		}else{
			
		}
		
	})
		
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="time"></div>
	<div id="title_right"></div>
	
	<div id="dashBoard_title">${account}</div>
	
	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/config_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/sky_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
				<td class="mainView" rowspan="10" id="svg_td" style="vertical-align: top;">
					<div id="wraper">
					
					</div>
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span" ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'> </span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	