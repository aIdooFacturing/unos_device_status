<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.common.min.css"/>
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.rtl.min.css"/>
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.silver.min.css"/>
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<script src="https://kendo.cdn.telerik.com/2018.1.221/js/kendo.all.min.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow: hidden;
	background-color: black;
  	font-family:'Helvetica';
}
#wraper{
	height : 80%;
}
.mainView{
	overflow: hidden;
	overflow-x:hidden;
}
.k-dialog-titlebar{
	display : none
}

</style> 
<script type="text/javascript">
	
	$(function(){
		
		getToday();
		
		createNav("config_nav", 2);
		setEl();
		
		var lang = window.localStorage.getItem("lang");
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999,
			"pointer-events": "none"
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"text-align" : "center",
			"color" : "white",
			"font-size" : getElSize(35),
			"padding" : getElSize(20),
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$("#wraper").css({
			"width" : "100%"
		})
		
		$(".tmpTable").css({
			"width" : "100%"			
		})
		
		$("#wraper").css({
			"height" : getElSize(1750)
		})
		
		$("#id-div").css({
			"position" : "absolute",
			"top" : getElSize(300),
			"left" : getElSize(600)
		})
		
		$("#id-input").val(window.sessionStorage.getItem("user_id"));
	
		$("#id-input").css({
			"font-size" : getElSize(100),
			"width" : getElSize(500)
		})
		
		$("#id-label").css({
			"color" : "white",
			"font-size" : getElSize(100)
		})
		
		$("#btn-div").css({
			"position" : "absolute",
			"bottom" : getElSize(300),
			"left" : getElSize(600),
			"width" : getElSize(2950)
		})
		
		$("#delete-btn").css({
			"font-size" : getElSize(100),
			"float" : "right"
		})
		
		$("#change-btn").css({
			"font-size" : getElSize(100)
		})
		
		$(".input-div").css({
			"height" : "50%"
		})
		
		$("#wraper").css({
			"color" : "white"
		})
		
		$(".action-btn").css({
			"font-size" : getElSize(70),
			"background" : "linear-gradient(#bfbfbf, #9e9e9e)",
			"color" : "rgb(34,34,34)",
			"border" : "1px solid white",
			"margin-left" : getElSize(50),
			"border-radius" : getElSize(10),
			"margin-top" : getElSize(50),
			"width" : getElSize(300)
		}).hover(function(){
			$(this).css({
				"color" : "white",
			})
		}, function(){
			$(this).css({
				"color" : "rgb(34,34,34)",
			})
		});
		
		
		$(".input-div").css({
			"height" : getElSize(100),
			"font-size" : getElSize(80),
			"margin-top" : getElSize(50),
			"margin-left" : getElSize(50)
		})
		
		$(".input").css({
			"font-size" : getElSize(80),
			"width" : getElSize(400)
		})
		
		$("#id-input, #level-input").css({
			"background" : "transparent",
			"color" : "white",	
			"border" : getElSize(0)
		})
		
		
		$(".k-dialog-titlebar").css({
			"display" : "none"
		})
		
		
		if(getParameterByName('lang')=='ko' || window.localStorage.getItem("lang")=="ko"){
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(100),
				"position" : "absolute",
				"top" : marginHeight + getElSize(85),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else if(getParameterByName('lang')=='en' || getParameterByName('lang')=='de' || window.localStorage.getItem("lang")=="de" || window.localStorage.getItem("lang")=="en"){
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(70),
				"position" : "absolute",
				"top" : marginHeight + getElSize(115),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else{
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(90),
				"position" : "absolute",
				"top" : marginHeight + getElSize(100),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}
		
		
	}
	
	$(function(){
		
		var id = window.sessionStorage.getItem("user_id");
		var level = window.sessionStorage.getItem("level");
		
		if(level==1){
			level="${user}"
		}else if(level==2){
			level="${manager}"
		}
		
		$("#id-input").val(id)
		$("#level-input").val(level)
		
	})
	
	function updateUser(){
		
		if($("#password-input").val().length>=5){
			var url = ctxPath + "/updateUserBasic.do";
			var param = "shopId=" + shopId +
						"&id=" + window.sessionStorage.getItem("user_id") +
						"&password=" + $("#password-input").val();
			
			console.log(url+"?"+param)
			
			$.ajax({
				url : url,
				data : param,
				type : "post",
				dataType : "text",
				success : function(data){
						kendo.alert("${save} ${complete}")
						
						$("#password-input").val("")
				}
			});
		}else{
			kendo.alert("${passwordAlert}")
		}
	}
		
</script>
</head>
<body>
	
	<div id="time"></div>
	<div id="title_right"></div>
	
	<div id="dashBoard_title">${account}</div>
	
	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/config_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/sky_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
				<td class="mainView" rowspan="10" id="svg_td" style="vertical-align: top;">
					<div id="wraper">
						<h1><spring:message  code="account"></spring:message></h1>
						<hr>
						<div class="input-div">
							<label><spring:message  code="id"></spring:message> : <input class="input" id="id-input" disabled type="text"></label>
						</div>
						<br>
						<div class="input-div">
							<label><spring:message  code="authority"></spring:message> : <input class="input" id="level-input" disabled type="text"></label>
						</div>
						<br>
						<div class="input-div">
							<label><spring:message  code="password"></spring:message> : <input class="input" id="password-input" type="password" maxlength="12"></label>
						</div>
						<br>
						<button class="action-btn" onclick="updateUser()"><spring:message  code="save"></spring:message></button>
					</div>
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'> </span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	