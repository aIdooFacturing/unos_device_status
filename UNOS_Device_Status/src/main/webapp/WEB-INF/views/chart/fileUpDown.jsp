<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
response.setContentType("text/html;charset=UTF-8");
response.setCharacterEncoding("UTF-8");
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.common.min.css"/>
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.rtl.min.css"/>
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.silver.min.css"/>
<link rel="stylesheet" href="${ctxPath }/css/jquery.loading.min.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<script src="https://kendo.cdn.telerik.com/2018.1.221/js/kendo.all.min.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery.loading.min.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
/* body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden;
	background-color: black;
  	font-family:'Helvetica';
} */
.k-grid tbody > tr
{
 background : #323232 !important;
 text-align: center;
 color: white !important;
}

.k-grid tbody > tr:hover
{
 background :  darkgray !important;
}
 
.k-grid tbody > .k-alt
{
 background :  #222222	 !important;
 text-align: center;
 color: white;
}
.k-grid-content{
 background : black;
}

/* .k-grid{
	border : 2px solid #323232;
	border-bottom : 0px;
} */
.k-grid th.k-header,
.k-grid-header
{
    background: linear-gradient( to bottom, gray, black ) !important;
    color : white !important;
    text-align: center;
}
div.k-grid-header{
 padding-right:0px !important;
}
#grid{
	display: inline-block !important;
}

.btn-area{
	display: inline;
	float: right;
}
/* #val {
    width: 400px;
    height:25px;
    position: absolute;
    top: 0;
    left: 0;
    font-size:13px;
    line-height: 25px;
    text-indent: 10px;
    pointer-events: none;
} */
#button {
    cursor: pointer;
    display: table-cell;
    vertical-align:middle;
    background: linear-gradient(#F7F7F7, #DDDDDD);
    color: black;
    position: absolute;
    text-align: center;
}

#button:hover {
    background-color: blue;
}

.border{
	border: 1px solid #A9A9A9
}

</style> 
<script type="text/javascript">
	var shopId=1;
	
	
	$(function(){
		
		location.href="elfinos://";
		
		getToday();
		
		createNav("config_nav", 6);
		setEl();
		
		var lang = window.localStorage.getItem("lang");
		
		getData();
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function getData(){
		var url = ctxPath + "/getDvcList.do";
		var param = "shopId=" + shopId;
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				
				for(var i=0;i<data.length;i++){
					var option = $("<option value="+data[i].dvcId+">"+data[i].name+"</option>");
					$("#dvcSelector").append(option);
				}
				
			}
		});
	};
	
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999,
			"pointer-events": "none"
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"text-align" : "center",
			"color" : "white",
			"font-size" : getElSize(35),
			"padding" : getElSize(20),
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		/* $("select, button, input").css({
			"font-size" : getElSize(50),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		}); */
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
		});
		
		$("#content_table td").css({
			"color" : "##BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#table2 td").css({
			"text-align" : "center",
			"color" : "white",
			"font-size" : getElSize(40),
			"padding" : getElSize(20),
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		

		$("#delDiv").css({
			"color" : "white",
			"z-index" : 9999999,
			"background-color" : "black",
			"position" : "absolute",
			"width" : getElSize(700) + "px",
			"font-size" : getElSize(60) + "px",
			"text-align" : "center",
			"padding" : getElSize(30) + "px",
			"border" : getElSize(7) + "px solid rgb(34,34,34)",
			"border-radius" : getElSize(50) + "px"
		});
		
		
		$("#delDiv div").css({
			"background-color" : "rgb(34,34,34)",
			"padding" : getElSize(20) + "px",
			"margin" : getElSize(10) + "px",
			"cursor" : "pointer"
		}).hover(function(){
			$(this).css({
				"background-color" : "white",
				"color" : "rgb(34,34,34)",
			})
		}, function(){
			$(this).css({
				"background-color" : "rgb(34,34,34)",
				"color" : "white",
			})
		});
		
		$("#delDiv").css({
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			"top" : - $("#delDiv").height() * 2
		});
		
		$("#delDiv div:nth(1)").click(function(){
			$("#delDiv").animate({
				"top" : - $("#delDiv").height() * 2
			});
		})
	
		$("#svg_td").css({
			"width" : getElSize(1000),
			"color " : "white"
		})
		
		$("#input-div").css({
			"width" : getElSize(1050),
			"height" : getElSize(100),
			"font-size" : getElSize(50),
			"position" : "absolute",
			"right" : getElSize(350),
			"top" : getElSize(140),
			"display" : "table",
			"background" : "white",
			"border": getElSize(1)+"px solid #d8d8d8",
			"box-shadow": getElSize(3)+"px"+getElSize(6)+"px"+getElSize(9)+"px #ededed" 
		})
		
		$("#button").css({
			"width" : getElSize(350),
			"height" : "100%",
			"font-size" : getElSize(50),
			/* "line-height" : getElSize(4), */
			"margin" : "0px",
			"border" : getElSize(3)+"px solid #A0A0A0",
			"top" : -getElSize(1),
			"right" : -getElSize(3)
		})
		
		
		$("#val").css({
			"font-size" : getElSize(50),
			"line-height" : getElSize(5)
		})
		
		$("#submit-btn").css({
			"width" : getElSize(300),
			"height" : getElSize(100),
			"font-size" : getElSize(50),
			"position" : "absolute",
			"right" : getElSize(0),
			"top" : getElSize(140)
		})
		
		/* $("input[type='file']").css({
			"width":getElSize(1000),
	    	"height": getElSize(70),
	    	"opacity":"0"
		}) */
		
		$("#fileList").css({
			"background" : "green",
			"height" : getElSize(1500)
		})
		
		$(".title").css({
			"color" : "white",
			"display" : "inline"
		})
		
		$("#fileName").css({
			"position" : "absolute",
			"left" : getElSize(1270),
			"top" : getElSize(300)
		})
		
		if(window.localStorage.getItem("lang")=="en" || window.localStorage.getItem("lang")=="de"){
			$("#button").css({
				"font-size":getElSize(30),
/* 				"line-height" : getElSize(8) */
			})
			
			
			
			
			$("#submit-btn").css({
				"font-size":getElSize(35)
			})
		}
		
		if(getParameterByName('lang')=='ko' || window.localStorage.getItem("lang")=="ko"){
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(100),
				"position" : "absolute",
				"top" : marginHeight + getElSize(85),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else if(getParameterByName('lang')=='en' || getParameterByName('lang')=='de' || window.localStorage.getItem("lang")=="en" || window.localStorage.getItem("lang")=="de"){
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(70),
				"position" : "absolute",
				"top" : marginHeight + getElSize(115),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else{
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(90),
				"position" : "absolute",
				"top" : marginHeight + getElSize(100),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}
		
		$("#wraper").css({
			"color" : "white"
		})
		
		$(".action-btn").css({
			"font-size" : getElSize(70),
			"background" : "linear-gradient(#bfbfbf, #9e9e9e)",
			"color" : "rgb(34,34,34)",
			"border" : "1px solid white",
			"margin-left" : getElSize(50),
			"border-radius" : getElSize(10),
			"margin-top" : getElSize(50),
			"width" : getElSize(330)
		}).hover(function(){
			$(this).css({
				"color" : "white",
			})
		}, function(){
			$(this).css({
				"color" : "rgb(34,34,34)",
			})
		});
		
		
	};
	
	
</script>

<script>
var fileList=[];

function getFileList(){
	
	
	$("#fileList").empty();
	
	var url = ctxPath + "/getFileList.do";
	var param = "shopId=" + shopId + "&dvcId=" + $("#dvcSelector").val();
		
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			var json = data.fileList;
			console.log(json)
			
			for(var i=0;i<json.length;i++){
				var option = $("<a href='resources/files/" + $("#dvcSelector").val()+"/"+json[i].file+"' download>"+ json[i].file +"</a><br>")
				$("#fileList").append(option)
			}
			
		}
	});
}





function fileUpload_Device() {
        
        var url = "${ctxPath}/fileUpload_Device.do";
        
        var formData = new FormData();
        formData.append('dvcId', $('#dvcSelector').val()); 
        formData.append('file', $('input[type=file]')[0].files[0]); 
        
       $('#svg_td').loading({
        	theme: 'dark',
        	message: '${saving}'
        });
       
       console.log(formData.get("dvcId"))
    
       if(window.sessionStorage.getItem("level")=='2'){
    	   $.ajax({ 
           	url: url, 
           	data: formData, 
           	processData: false, 
           	contentType: false, 
           	type: 'POST', 
           	success: function (response) { 
           		console.log(response); 
           		$('#svg_td').loading('stop');
           		location.reload();
           	}, error: function (jqXHR) { 
           		console.log('error'); 
           		$('#svg_td').loading('stop');
           		location.reload();
           	} 
           });
       }else{
    	   alert("권한이 없습니다.");
    	   $('#svg_td').loading('stop');
       }

    }
    
	$(function(){
		$('#button').click(function () {
		    $("input[type='file']").trigger('click');
		})
	
		$("input[type='file']").change(function () {
		    $('#val').text(this.value.replace(/C:\\fakepath\\/i, ''))
		})
		
		$("#dvcSelector").change(function(){
			if($("#dvcSelector").val()!="0"){
				
				getFileList();
			}
		})
		
		$("#selectFile").change(function(){
			  console.log(this.files[0].name);
			  $("#fileName").empty()
			  $("#fileName").css({
					"position" : "absolute",
					"left" : getElSize(1270),
					"top" : getElSize(300)
				})
			  $("#fileName").append(this.files[0].name)
		});
	})
	
	function downloadApp(){
		
		//kendo.alert("${passwordAlert}")
		location.href="https://elfinos.io:7443/files/iDOO_file_transfer-1.0.0-win.zip"
	}

</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="time"></div>
	<div id="title_right"></div>
	
	<div id="dashBoard_title">${fileUpDown}</div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/config_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/sky_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
				<!-- <td rowspan="10" id="svg_td" style="vertical-align: top;">
				
					<p class="title">장비 선택 :</p> 
					<select id="dvcSelector">
						<option value="0">====Device Select====</option>
					</select>
					
					<input type="file" id="selectFile">
					<div class="title" id="fileName"></div>
					<button onclick="fileUpload_Device()">업로드</button>
					
					<div id="fileList">
					</div>
				</td> -->
				
				<td class="mainView" rowspan="10" id="svg_td" style="vertical-align: top;">
					<div id="wraper">
						<h1><spring:message  code="fileUpDown"></spring:message></h1>
						<hr>
						<button class="action-btn" onclick="downloadApp()"><spring:message  code="download"></spring:message></button>
					</div>
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span" ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'> </span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'> </span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'> </span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'> </span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'> </span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'> </span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	