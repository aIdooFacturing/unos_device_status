<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
response.setContentType("text/html;charset=UTF-8");
response.setCharacterEncoding("UTF-8");
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.common.min.css"/>
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.rtl.min.css"/>
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.silver.min.css"/>
<link rel="stylesheet" href="${ctxPath }/css/jquery.loading.min.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
	
	// 로그인 용도 (삭제해야 함!!)
	/* window.sessionStorage.setItem("level", "2");
	window.sessionStorage.setItem("login", "success");	 
	window.sessionStorage.setItem("login_time", "1554375147421");
	window.sessionStorage.setItem("user_id", "admin"); */
	
</script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<script src="https://kendo.cdn.telerik.com/2018.1.221/js/kendo.all.min.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery.loading.min.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
/* body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden;
	background-color: black;
  	font-family:'Helvetica';
} */
.k-grid tbody > tr
{
 background : #323232 !important;
 text-align: center;
 color: white !important;
}

.k-grid tbody > tr:hover
{
 background :  darkgray !important;
}
 
.k-grid tbody > .k-alt
{
 background :  #222222	 !important;
 text-align: center;
 color: white;
}
.k-grid-content{
 background : black;
}

/* .k-grid{
	border : 2px solid #323232;
	border-bottom : 0px;
} */
.k-grid th.k-header,
.k-grid-header
{
    background: linear-gradient( to bottom, gray, black ) !important;
    color : white !important;
    text-align: center;
}
div.k-grid-header{
 padding-right:0px !important;
}
#grid{
	display: inline-block !important;
}

.btn-area{
	display: inline;
	float: right;
}
/* #val {
    width: 400px;
    height:25px;
    position: absolute;
    top: 0;
    left: 0;
    font-size:13px;
    line-height: 25px;
    text-indent: 10px;
    pointer-events: none;
} */
#button {
    cursor: pointer;
    display: table-cell;
    vertical-align:middle;
    background: linear-gradient(#F7F7F7, #DDDDDD);
    color: black;
    position: absolute;
    text-align: center;
}

#button:hover {
    background-color: blue;
}

.border{
	border: 1px solid #A9A9A9
}

</style> 
<script type="text/javascript">
	var shopId=1;
	
	
	$(function(){
		
		getToday();
		
		createNav("config_nav", 0);
		setEl();
		
		var lang = window.localStorage.getItem("lang");
		
		getData();
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	
	 var crudServiceBaseUrl = "//demos.telerik.com/kendo-ui/service";
	 
	function getData(){
		var url = ctxPath + "/getFacilitiesStatus.do";
		var param = "shopId=" + shopId;
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				
			}
		});
	};
	
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999,
			"pointer-events": "none"
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"text-align" : "center",
			"color" : "white",
			"font-size" : getElSize(35),
			"padding" : getElSize(20),
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(50),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
		});
		
		$("#content_table td").css({
			"color" : "##BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#table2 td").css({
			"text-align" : "center",
			"color" : "white",
			"font-size" : getElSize(40),
			"padding" : getElSize(20),
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		

		$("#delDiv").css({
			"color" : "white",
			"z-index" : 9999999,
			"background-color" : "black",
			"position" : "absolute",
			"width" : getElSize(700) + "px",
			"font-size" : getElSize(60) + "px",
			"text-align" : "center",
			"padding" : getElSize(30) + "px",
			"border" : getElSize(7) + "px solid rgb(34,34,34)",
			"border-radius" : getElSize(50) + "px"
		});
		
		
		$("#delDiv div").css({
			"background-color" : "rgb(34,34,34)",
			"padding" : getElSize(20) + "px",
			"margin" : getElSize(10) + "px",
			"cursor" : "pointer"
		}).hover(function(){
			$(this).css({
				"background-color" : "white",
				"color" : "rgb(34,34,34)",
			})
		}, function(){
			$(this).css({
				"background-color" : "rgb(34,34,34)",
				"color" : "white",
			})
		});
		
		$("#delDiv").css({
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			"top" : - $("#delDiv").height() * 2
		});
		
		$("#delDiv div:nth(1)").click(function(){
			$("#delDiv").animate({
				"top" : - $("#delDiv").height() * 2
			});
		})
	
		$("#svg_td").css({
			"width" : getElSize(1000)
		})
		
		$("#grid").css({
			"width" : getElSize(3330)
		})
		
		$("#input-div").css({
			"width" : getElSize(1050),
			"height" : getElSize(100),
			"font-size" : getElSize(50),
			"position" : "absolute",
			"right" : getElSize(350),
			"top" : getElSize(140),
			"display" : "table",
			"background" : "white",
			"border": getElSize(1)+"px solid #d8d8d8",
			"box-shadow": getElSize(3)+"px"+getElSize(6)+"px"+getElSize(9)+"px #ededed" 
		})
		
		$("#button").css({
			"width" : getElSize(350),
			"height" : "100%",
			"font-size" : getElSize(50),
			/* "line-height" : getElSize(4), */
			"margin" : "0px",
			"border" : getElSize(3)+"px solid #A0A0A0",
			"top" : -getElSize(1),
			"right" : -getElSize(3)
		})
		
		
		$("#val").css({
			"font-size" : getElSize(50),
			"line-height" : getElSize(5)
		})
		
		$("#submit-btn").css({
			"width" : getElSize(300),
			"height" : getElSize(100),
			"font-size" : getElSize(50),
			"position" : "absolute",
			"right" : getElSize(0),
			"top" : getElSize(140)
		})
		
		$("input[type='file']").css({
			"width":getElSize(1000),
	    	"height": getElSize(70),
	    	"opacity":"0"
		})
		
		if(getParameterByName('lang')=='ko' || window.localStorage.getItem("lang")=="ko"){
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(100),
				"position" : "absolute",
				"top" : marginHeight + getElSize(85),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else if(getParameterByName('lang')=='en' || getParameterByName('lang')=='de' || window.localStorage.getItem("lang")=="de" || window.localStorage.getItem("lang")=="en"){
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(70),
				"position" : "absolute",
				"top" : marginHeight + getElSize(115),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else{
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(90),
				"position" : "absolute",
				"top" : marginHeight + getElSize(100),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}
		
		if(window.localStorage.getItem("lang")=="en" || window.localStorage.getItem("lang")=="de"){
			$("#button").css({
				"font-size":getElSize(30),
/* 				"line-height" : getElSize(8) */
			})
			
			$("#submit-btn").css({
				"font-size":getElSize(35)
			})
		}
		
		$(".fit").css({
			"height" : getElSize(100),
			"width" : getElSize(100)
		})
		
		if(window.sessionStorage.getItem("level")!=2){
			$("#submit-btn, #input-div").css({
				"display" : "none"
			})
			
		}
		
	};
	
	
	function readOnly(container, options) {
        container.removeClass("k-edit-cell");
        container.text(options.model.get(options.field));
    }
	
	
</script>

<script>
var selectlist=[];
var mcTyList=[];
var groupList=[];
var picList=[];

function getJIGList(){
	var url = ctxPath + "/getJIGList.do";
	var param = "shopId=" + shopId;
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			var json = data.dataList;
			$(json).each(function(idx, data){
				selectlist.push(decode(data.JIG))
			});
		}
	});
}

function getMCTYList(){
	var url = ctxPath + "/getMCTYList.do";
	var param = "shopId=" + shopId;
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			var json = data.dataList;
			$(json).each(function(idx, data){
				mcTyList.push(decode(data.mcTy))
			});
		}
	});
}

function getGroup(){
	var url = ctxPath + "/getGroup.do";
	var param = "shopId=" + shopId;
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			var json = data.dataList;
			$(json).each(function(idx, data){
				groupList.push(decode(data.group))
			});
		}
	});
}

function getPicList(){
	var url = ctxPath + "/getImageList.do";
	$.ajax({
		url : url,
		type : "post",
		dataType : "json",
		success : function(data){
			var json = data.fileList;
			$(json).each(function(idx, data){
				var pic ={}
				pic.id=idx+1
				pic.name=data.file
				
				
				var url = "${ctxPath}/checkStatusFile.do";
	        	var param = "fileName=" + data.file;
	        	
	        	 $.ajax({ 
	             	url: url, 
	             	data: param, 
	            	dataType : "text",
	             	type: 'POST', 
	             	async: false,
	             	success: function (response) { 
	             		
	             		var obj = JSON.parse(response)
	             		pic.condition=obj.hasCondition.toString();
	             		
	             	}, error: function (jqXHR) { 
	             		
	             	} 
	             });
	        	 
	        	 picList.push(pic)
				
				
				
			});
			
		}
	});
}

function JIGList(container, options, a){
	autoComplete = $('<input data-bind="value:' + options.field + '"/>')
	.appendTo(container)
	.kendoComboBox({
    	dataSource: selectlist,
		autoWidth: true,
        select:function(e){
        }
    }).data("kendoComboBox");
}

function mcTyListComplete(container, options, a){

	$('<input data-bind="value:' + options.field + '"/>')
	.appendTo(container)
	.kendoComboBox({
    	dataSource: mcTyList,
		autoWidth: true,
        select:function(e){
        }
    }).data("kendoComboBox");
}

function groupListComplete(container, options, a){

	$('<input data-bind="value:' + options.field + '"/>')
	.appendTo(container)
	.kendoComboBox({
    	dataSource: groupList,
		autoWidth: true,
        select:function(e){
        }
    }).data("kendoComboBox");
}

function picListComplete(container, options, a){
	
	var dataSource11 = new kendo.data.DataSource({
		data: picList,
		type: "json"
	})

	var picListComboBox = $('<input data-bind="value:' + options.field + '"/>')
	.appendTo(container)
	.kendoComboBox({
    	dataSource: dataSource11,
    	headerTemplate: '<div class="dropdown-header k-widget k-header">' +
							'<div class="inline left-title">번호</div> <div class="inline center-title">그림</div><div class="inline right-title">파일명</div><div class="inline last-title">표시가능상태</div>' +
						'</div>',
    	footerTemplate : "<div>총 #:instance.dataSource.total()# 개</div>",
    	template : '<div class="center first">#:data.id#</div>'+
    				'<img class="fit border" src="/Device_Status/resources/upload/#:data.name#_IN-CYCLE.svg">'+
    				'<div class="center">#:data.name#</div>'+
    				'<div class="center last">#:data.condition#</div>',
    	dataValueField: "name",
    	dataTextField: "name",
    	dataBound:function(e){
    		$(".fit").css({
    			"height" : getElSize(150),
    			"width" : getElSize(150),
    			"padding" : getElSize(20),
    			"background" : "#B9C0CB",
    			"margin-top" : getElSize(10),
    			"margin-bottom" : getElSize(-10)
    		})
    		
    		$(".center").css({
    			"display" : "inline",
    			"position" : "relative",
    			"bottom" : getElSize(70),
    			"margin-left" : getElSize(100)
    		})
    		
    		$(".first").css({
    			"margin-left" : getElSize(30),
    			"margin-right" : getElSize(30),
    			"width" : getElSize(30)
    		})
    		
    		$(".inline").css({
    			"display" : "inline",
    			"padding" : getElSize(30)
    		})
    		
    		$(".left-title").css({
    			"margin-left" : getElSize(-10)
    		})
    		
    		$(".center-title").css({
    			"margin-left" : getElSize(-10)
    		})
    		
    		$(".right-title").css({
    			"margin-left" : getElSize(100)
    		})
    		
    		$(".last-title").css({
    			"margin-left" : getElSize(300)
    		})
    		
    		$(".k-animation-container").css({
    			"height" : getElSize(1000)
    		})
    		
    		$(".last").css({
    			"word-break" : "normal",
    			"white-space" : "normal",
    			"width" : getElSize(100)
    		})
    	
    		
    	},
		autoWidth: true,
		height: 500,
        select:function(e){
        }
    }).data("kendoComboBox");
}

function showSelector(container, options, a){

	$('<input data-bind="value:' + options.field + '"/>')
	.appendTo(container)
	.kendoDropDownList({
		 dataSource: [
			    { id: 0, name: "${hide}" },
			    { id: 1, name: "${show}" }
	  	],
	  	dataTextField: "name",
	    dataValueField: "id",
        select:function(e){
        }
    }).data("kendoDropDownList");
}

function useSelector(container, options, a){

	$('<input data-bind="value:' + options.field + '"/>')
	.appendTo(container)
	.kendoDropDownList({
		 dataSource: [
			    { id: 0, name: "${notUse}" },
			    { id: 1, name: "${use}" }
	  	],
	  	dataTextField: "name",
	    dataValueField: "id",
        select:function(e){
        }
    }).data("kendoDropDownList");
}



var grid;
$(function(){
	
	getJIGList();
	getMCTYList();
	getGroup();
	getPicList();
	
	kendodata = new kendo.data.DataSource({
		 transport: {
            read: {
            	 url: ctxPath + "/getFacilitiesStatus.do?shopId="+shopId,
                 dataType: "json",
                 type: "GET",
                 data:function(e){
                	 console.log(e)
                 }
            },
            destroy: {
            	url: ctxPath + "/deleteDevice.do",
                dataType: "json",
                type: "POST",
            },
            update: {
            	url: ctxPath + "/updateDevice.do",
            	dataType: "json"
            },
            create: {
            	url: ctxPath + "/createDevice.do",
            	dataType: "json"
            },
            parameterMap: function(options, operation) {
				return {
					models: kendo.stringify(options)
					};
            }
        },
       /*  change: function(e){
            if(e.action == "itemchange"){
                e.items[0].dirtyFields = e.items[0].dirtyFields || {};
                e.items[0].dirtyFields[e.field] = true;
            }
        }, */
        batch: false,
       schema: {
           model: {
               id: "dvcId",
               fields: {
               	dvcId: { editable: true, nullable: true, type: "number" },
               	name: { editable: true, nullable: false,
               		validation: {
                        required: true,
                        maxlength: 30
                	} 
               	},
               	wc: { editable: true, nullable: true,
               		validation: {
                        required: true,
                        maxlength: 30
                	}	
               	},
               	jig: { editable: true, nullable: true, 
               		validation: {
                        required: true,
                        maxlength: 30
                	}	
               	},
               	mcTy: { editable: true, nullable: true,
               		validation: {
                        required: true,
                        maxlength: 30
                	}
               	},
               	ncTy: { editable: true, nullable: true,
               		validation: {
                        required: true,
                        maxlength: 30
                	}
               	},
               	group: { editable: true, nullable: true,
               		validation: {
                        required: true,
                        maxlength: 30
                	}
               	},
               	cavity: { editable: true, nullable: false, type: "number",
               		validation: {
                        required: true,
                        maxlength:
	                        function(input) { 
	                            if (input.is("[name=cavity]")) {
	                            	if($("input[name=cavity]").val()>9){
	                            		 input.attr("data-maxlength-msg","Cavity 의 최대 크기는 9 입니다.");
	  	                               return false;
	                            	}
	                            }                                   
	                            return true;
	                        }
                	}
               	},
               	night: { editable: true, nullable: false, type: "number" },
               	showing: { editable: true, nullable: false, type: "number" },
               	pic: { editable: true, nullable: false },
               	h: { editable: true, nullable: false, type: "number",
                	validation: {
                        required: true,
                        maxlength: 3
	                        
                	}		
                },
                w: { editable: true, nullable: false, type: "number",
                	validation: {
                        required: true,
                        maxlength:
	                        function(input) { 
	                            if (input.is("[name=w]")) {
	                            	if($("input[name=w]").val()>500){
	                            		 input.attr("data-maxlength-msg","가로의 최대 크기는 500 입니다.");
	  	                               return false;
	                            	}
	                            }                                   
	                            return true;
	                        }
                	}		
                },
                shopId: { defaultValue: 1, editable: false, nullable: false, type: "number"}
               }
           }
       }
	})
	
	kendodata.fetch(function() {
        kendodata.sync();
    })
	
    console.log(kendodata)
	
	if(window.sessionStorage.getItem("level")=='2'){
		grid =  $("#grid").kendoGrid({
			dataSource: kendodata,
			height : getElSize(1780),
			scrollable:true,
			cancel: function(e) {
				e.sender.refresh();
				e.preventDefault();
				$(".k-grid-edit, .k-grid-delete, .k-grid-update, .k-grid-cancel").css({
					"font-size":getElSize(40)
				})
			},
			dataBound :function(e){
				
				$(".k-grid-edit, .k-grid-delete, .k-grid-update, .k-grid-cancel").css({
					"font-size":getElSize(40)
				})
				
				$(".k-grid-add").css({
					"font-size":getElSize(40)
				})
				
					
	    		$(".k-grid tbody tr").css({
					"height" : getElSize(110)
				})
				
			},
			edit: function(e){
				
				$(".k-grid-edit, .k-grid-delete, .k-grid-update, .k-grid-cancel").css({
					"font-size":getElSize(40)
				})
			},
			editable: { 
				mode: "inline",
				confirmation: "${askDeletion}"
			},
			toolbar: [{ name: "create" , text:'${device} ${add}'}],
			 columns: [
				 {
				    field: "dvcId",
				    title: "${device}${number}",
				    width : getElSize(400),
				    locked: true,
				    attributes: {
		        	      style: "font-size:"+getElSize(40),
		      	    },headerAttributes: {
		      	    	 style: "font-size:"+getElSize(40),
		      	    }
				  },{
				    field: "name",
				    title: "${device}",
				    width : getElSize(400),
				    locked: true,
				    attributes: {
		        	      style: "font-size:"+getElSize(40),
		      	    },headerAttributes: {
		      	    	 style: "font-size:"+getElSize(40),
		      	    }
				  },{
				    field: "wc",
				    title: "WC",
				    width : getElSize(400),
				    attributes: {
		        	      style: "font-size:"+getElSize(40),
		      	    },headerAttributes: {
		      	    	 style: "font-size:"+getElSize(40),
		      	    }
				  },{
				    field: "jig",
				    title: "JIG",
				    editor: JIGList,
				    width : getElSize(400),
				    attributes: {
		        	      style: "font-size:"+getElSize(40),
		      	    },headerAttributes: {
		      	    	 style: "font-size:"+getElSize(40),
		      	    }
				  },{
				    field: "mcTy",
				    title: "MC_TYPE",
				    editor: mcTyListComplete,
				    width : getElSize(400),
				    attributes: {
		        	      style: "font-size:"+getElSize(40),
		      	    },headerAttributes: {
		      	    	 style: "font-size:"+getElSize(40),
		      	    }
				  },{
				    field: "ncTy",
				    title: "NC_TYPE",
				    width : getElSize(400),
				    attributes: {
		        	      style: "font-size:"+getElSize(40),
		      	    },headerAttributes: {
		      	    	 style: "font-size:"+getElSize(40),
		      	    }
				  },{
				    field: "group",
				    title: "GROUP",
				    width : getElSize(400),
				    editor: groupListComplete,
				    attributes: {
		        	      style: "font-size:"+getElSize(40),
		      	    },headerAttributes: {
		      	    	 style: "font-size:"+getElSize(40),
		      	    }
				  },{
				    field: "cavity",
				    title: "CAVITY",
				    width : getElSize(400),
				    attributes: {
		        	      style: "font-size:"+getElSize(40),
		      	    },headerAttributes: {
		      	    	 style: "font-size:"+getElSize(40),
		      	    }
				  },{
				    field: "night",
				    title: "${dmm}",
				    width : getElSize(400),
				    template: function(dataItem) {
				        if(dataItem.night==0){
				        	return "${notUse}"
				        }else{
				        	return "${use}"
				        }
			      	},
				    editor: useSelector,
				    attributes: {
		        	      style: "font-size:"+getElSize(40),
		      	    },headerAttributes: {
		      	    	 style: "font-size:"+getElSize(40),
		      	    }
				  },{
				    field: "pic",
				    title: "${picture}",
				    width : getElSize(400),
				    editor: picListComplete,
				    attributes: {
		        	      style: "font-size:"+getElSize(40),
		      	    },headerAttributes: {
		      	    	 style: "font-size:"+getElSize(40),
		      	    }
				  },{
				    field: "h",
				    title: "${h}",
				    width : getElSize(400),
				    attributes: {
		        	      style: "font-size:"+getElSize(40),
		      	    },headerAttributes: {
		      	    	 style: "font-size:"+getElSize(40),
		      	    }
				  },{
				    field: "w",
				    title: "${w}",
				    width : getElSize(400),
				    attributes: {
		        	      style: "font-size:"+getElSize(40),
		      	    },headerAttributes: {
		      	    	 style: "font-size:"+getElSize(40),
		      	    }
				  },
				  {
					    field: "cutLevel",
					    title: "CUT LEVEL",
					    width : getElSize(400),
					    attributes: {
			        	      style: "font-size:"+getElSize(40),
			      	    },headerAttributes: {
			      	    	 style: "font-size:"+getElSize(40),
			      	    }
				  },
				  {
				    field: "showing",
				    title: "${visibility}",
				    width : getElSize(400),
				    template: function(dataItem) {
				        if(dataItem.showing==0){
				        	return "${hide}"
				        }else{
				        	return "${show}"
				        }
			      	},
				    editor:showSelector,
				    attributes: {
		        	      style: "font-size:"+getElSize(40),
		      	    },headerAttributes: {
		      	    	 style: "font-size:"+getElSize(40),
		      	    }
				  },
				  { command: [{ name: "edit", text: { update: '${modify} ${compl}', edit : '${modify}',cancel: "${modify} ${cancel}",} }, { name: "destroy", text: '${del}' }], title: "&nbsp;", width: getElSize(550)+"px" }
			  ]
		 }).data("kendoGrid");
	}else{
		grid =  $("#grid").kendoGrid({
			dataSource: kendodata,
			height : getElSize(1600),
			 columns: [
				 {
				    field: "name",
				    title: "${device}",
				    width : getElSize(400),
				    attributes: {
		        	      style: "font-size:"+getElSize(40),
		      	    },headerAttributes: {
		      	    	 style: "font-size:"+getElSize(43),
		      	    }
				  },{
				    field: "wc",
				    title: "WC",
				    width : getElSize(400),
				    attributes: {
		        	      style: "font-size:"+getElSize(40),
		      	    },headerAttributes: {
		      	    	 style: "font-size:"+getElSize(43),
		      	    }
				  },{
				    field: "jig",
				    title: "JIG",
				    editor: JIGList,
				    width : getElSize(400),
				    attributes: {
		        	      style: "font-size:"+getElSize(40),
		      	    },headerAttributes: {
		      	    	 style: "font-size:"+getElSize(43),
		      	    }
				  },{
				    field: "mcTy",
				    title: "MC_TYPE",
				    editor: mcTyListComplete,
				    width : getElSize(400),
				    attributes: {
		        	      style: "font-size:"+getElSize(40),
		      	    },headerAttributes: {
		      	    	 style: "font-size:"+getElSize(43),
		      	    }
				  },{
				    field: "ncTy",
				    title: "NC_TYPE",
				    width : getElSize(400),
				    attributes: {
		        	      style: "font-size:"+getElSize(40),
		      	    },headerAttributes: {
		      	    	 style: "font-size:"+getElSize(43),
		      	    }
				  },{
				    field: "group",
				    title: "GROUP",
				    editor: groupListComplete,
				    attributes: {
		        	      style: "font-size:"+getElSize(40),
		      	    },headerAttributes: {
		      	    	 style: "font-size:"+getElSize(43),
		      	    }
				  },{
				    field: "cavity",
				    title: "CAVITY",
				    attributes: {
		        	      style: "font-size:"+getElSize(40),
		      	    },headerAttributes: {
		      	    	 style: "font-size:"+getElSize(43),
		      	    }
				  }
			  ]
		 }).data("kendoGrid");
	}
	
	
	
	$("#insert-row").on("click", function(e) {
        var index = 0; //random hard-coded index
        var grid = $("#grid").data("kendoGrid");
        var dataSource = grid.dataSource;
        var newItem = dataSource.insert(index, {});
        var newRow = grid.items().filter("[data-uid='" + newItem.uid + "']");
        grid.editRow(newRow);
      });
	
	
	$(".k-grid-edit").css({
		"font-size":getElSize(20)
	})
	
})



function fileSubmit() {
        
        var url = "${ctxPath}/imageUpload_Device.do";
        
        var formData = new FormData();
        formData.append('image', $('input[type=file]')[0].files[0]); 
        
       $('#svg_td').loading({
        	theme: 'dark',
        	message: '${saving}'
        });
    
       if(window.sessionStorage.getItem("level")=='2'){
    	   $.ajax({ 
           	url: url, 
           	data: formData, 
           	processData: false, 
           	contentType: false, 
           	type: 'POST', 
           	success: function (response) { 
           		console.log(response); 
           		$('#svg_td').loading('stop');
           		location.reload();
           	}, error: function (jqXHR) { 
           		console.log('error'); 
           		$('#svg_td').loading('stop');
           		location.reload();
           	} 
           });
       }else{
    	   alert("권한이 없습니다.");
    	   $('#svg_td').loading('stop');
       }

    }
    
	$(function(){
		$('#button').click(function () {
		    $("input[type='file']").trigger('click');
		})
	
		$("input[type='file']").change(function () {
		    $('#val').text(this.value.replace(/C:\\fakepath\\/i, ''))
		})
	})

</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="dashBoard_title">${machine_list}</div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home">
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/config_left.png" class='menu_left'>
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/sky_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top;">
				
					<button id="submit-btn" onclick="fileSubmit()" ><spring:message code="transferFile"/></button>
				
					<div id="input-div">
						<input type="file" id="input_file" accept="image/svg+xml">  
						<p id='val'></p>
						<p id='button'><spring:message code="selectDeviceFile"></spring:message></p>
					</div>
				
					<div id="grid"></div>
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'> </span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'> </span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'> </span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'> </span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'> </span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'> </span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	